<?php 

$base_path = $_SERVER[ 'PHP_SELF'];
$base_path = str_replace( '/index.php', '', $base_path );

?>

<!DOCTYPE html>
<html class="portfolio">

  <head>

    <title>Scott Ashmore - Developer</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,400,300,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo $base_path; ?>/css/app.css">
    <link rel="stylesheet" href="<?php echo $base_path; ?>/css/sprite.css">

    <script src="<?php echo $base_path; ?>/js/vendor.js"></script>

    <script>
      var env = {
       BASE_PATH: '<?php echo $base_path; ?>',
       DEBUG: true
      };
    </script>

  </head>

  <body class="roboto">

    <div id="loader" class="layer layer-13">
      <div class="percent"></div>
      <div class="loading-bars">
        <div class="bar bar-odd bar-1"></div>
        <div class="bar bar-even bar-2"></div>
        <div class="bar bar-odd bar-3"></div>
        <div class="bar bar-even bar-4"></div>
      </div>
    </div>

    <header id="header"></header>
    
    <main id="main"></main>

    <footer id="footer"></footer>

    <script src="<?php echo $base_path; ?>/js/app.js"></script>

  </body>

</html>