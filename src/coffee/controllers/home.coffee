dictionary    = require 'models/dictionary'
settings      = require 'app/settings'
loader        = require 'utils/loading/loader'
utils         = require 'utils/utils'

Model         = require 'models/projects'
View          = require 'views/home'
AppController = require 'controllers/app_controller'

class HomeController extends AppController

	run: ( callback ) ->

		###
		template
		###
		template = require 'templates/pages/home.jade'

		locals =
			base_path  : settings.base_path
			dictionary : dictionary
			data       : do Model.get

		@ui.main.append template locals

		@view   = new View
		@loader = new loader

		@animate_load_progress @loader

		@loader.once 'loaded', =>

			@view.start $ '#home'

			do callback

		do @load


	load: ->

		for project in Model.get()

			for asset in project.assets

				if asset.id is 'big-home-image'

					@loader.add asset.id, settings.base_path + '/' + asset.src, asset.type

		do @loader.load


module.exports = new HomeController