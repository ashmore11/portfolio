dictionary    = require 'models/dictionary'
settings      = require 'app/settings'
loader        = require 'utils/loading/loader'
utils         = require 'utils/utils'

Model         = require 'models/projects'
View          = require 'views/project'
AppController = require 'controllers/app_controller'

class ProjectController extends AppController

	run: ( callback ) ->

		###
		template
		###
		template = require 'templates/pages/project.jade'

		locals =
			base_path  : settings.base_path
			dictionary : dictionary
			data       : do @get_project

		@ui.main.append template locals

		@view   = new View
		@loader = new loader

		@animate_load_progress @loader

		@loader.once 'loaded', =>

			@view.start $ '#project'

			do callback

		do @load


	load: ->

		for asset in @get_project().assets

			unless asset.id is 'big-home-image'

				@loader.add asset.id, settings.base_path + '/' + asset.src, asset.type

		do @loader.load


	get_project: ->

		url    = window.location.pathname.replace settings.base_path, ''
		pieces = url.split('/')
		id     = pieces[ pieces.length - 1 ]

		for project in Model.get()

			if project.id is id

				return project


module.exports = new ProjectController