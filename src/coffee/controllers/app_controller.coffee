utils = require 'utils/utils'

module.exports = class AppController

	constructor: ->

		@el = $ 'body'
		@ui =
			'main'     : '#main'
			'header'   : '#header'
			'footer'   : '#footer'
			'load_bar' : '.loading-bars'
			'odd_bar'  : '.loading-bars .bar-odd'
			'even_bar' : '.loading-bars .bar-even'

		@ui[ key ] = @el.find( id ) for key, id of @ui


	animate_load_progress: ( loader ) ->
		
		@ui.odd_bar.css  width: 0
		@ui.even_bar.css height: 0

		loader.on 'loading', ( percent ) =>

			$( '#loader .percent' ).html Math.floor( percent ) + '%'

			@ui.load_bar.show()

			params_1 = 
				width: percent + '%'
				ease : Power4.easeInOut

			params_2 = 
				height: percent + '%'
				ease : Power4.easeInOut

			if percent is 100

				utils.delay 1000, =>

					@ui.load_bar.fadeOut()

			TweenLite.to @ui.odd_bar,  1, params_1
			TweenLite.to @ui.even_bar, 1, params_2
		

	destroy: ( callback ) ->

		@view.once 'destroyed', =>

			@view = null
			
			delete @view

			do callback

		do @view.destroy