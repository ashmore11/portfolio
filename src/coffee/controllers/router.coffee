happens  = require 'happens'
device   = require 'app/device'
routes   = require 'models/routes'
settings = require 'app/settings'
utils    = require 'utils/utils'
loader   = require 'components/loader'

module.exports = new class Router

	_url: String
	previous_route_index: -1
	route_index: -1

	constructor: ->

		happens @

		History.Adapter.bind window, "statechange", @on_state_change

		###
		Initialise the routes
		###
		@routes = routes.get()


	start: ->

		do @on_state_change


	on_state_change: =>

		State = do History.getState
		@_url = do @get_url
		
		@previous_route_index = @route_index
		
		for route, index in routes.get()

			if route.url.match @_url
				
				@route_index = index
				
				break

		@date = new Date

		if @routes[ @previous_route_index ]?

			do loader.show

			loader.once 'shown', =>
			
				@routes[ @previous_route_index ].controller.destroy =>
					
					do @run

		else

			do @run

		@emit 'url:changed', @_url


	run: ->

		@routes[ @route_index ].controller.run =>

			do loader.hide
		

	get_url: -> 

		url    = window.location.pathname.replace settings.base_path, ''
		pieces = url.split('/')

		switch pieces.length

			when 2, 3
				return '/' + pieces[1]

			when 1
				return '/' + pieces[0]


	go: ( url ) ->

		return if url is @_url

		pieces = url.split('/')

		if pieces[2] is 'project' then delay = 1000 else delay = 0

		utils.delay delay, ->

			if device.history
				History.pushState null, null, url
			else
				window.location = url


	back: ->

		do History.back
