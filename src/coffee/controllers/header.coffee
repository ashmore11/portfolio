dictionary    = require 'models/dictionary'
settings 	    = require 'app/settings'

View 		      = require 'views/header'
AppController = require 'controllers/app_controller'

class HeaderController extends AppController

	constructor: ->

		super()

		template = require 'templates/partials/header.jade'

		locals =
			base_path  : settings.base_path
			dictionary : dictionary
			data       : {}

		@ui.header.append template locals

		@view = new View $ '#header'
		

module.exports = new HeaderController