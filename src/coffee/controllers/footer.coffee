dictionary    = require 'models/dictionary'
settings 	    = require 'app/settings'

View 		      = require 'views/footer'
AppController = require 'controllers/app_controller'

class FooterController extends AppController
					
	constructor: ->

		super()

		template = require 'templates/partials/footer.jade'

		locals =
			base_path  : settings.base_path
			dictionary : dictionary
			data       : {}

		@ui.footer.append template locals

		@view = new View @ui.footer


module.exports = new FooterController