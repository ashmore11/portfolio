log        = require 'utils/log'
settings   = require 'app/settings'
dictionary = require 'models/dictionary'

class APP

	constructor: ->

		# Expose the app
		window.APP = @

		# Debug
		window.c = log
		c.enable = settings.debug

		dictionary.once 'loaded', @start
		do dictionary.load

	start: =>

		###
		Run static views
		###
		require 'controllers/header'
		require 'controllers/footer'

		router = require 'controllers/router'
		do router.start	


module.exports = $ -> new APP