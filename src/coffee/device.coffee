module.exports = new class Device

	constructor: ->

		@retina  = window.devicePixelRatio is 2
		@history = Modernizr.history
		@ie 	   = bowser.msie
		@firefox = bowser.firefox
		@ltie10	 = bowser.msie and bowser.version < 10
		@ltie9	 = bowser.msie and bowser.version < 9
		@name    = bowser.name
		@version = bowser.version

		if @ltie10
			Modernizr.csstransforms = off

		cls = @name.split(' ').join('_') + '_' + @version.split('.').join '_'

		$('html').addClass cls

		if @ltie9
			$('html').addClass 'no-svg'