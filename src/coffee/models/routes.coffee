dictionary = require 'models/dictionary'

class Route

	id         : String
	url        : String
	controller : Object

module.exports = new class Routes

	get: ->

		@routes = []

		xml = dictionary.get_dictionary('routes.xml')

		xml.find('route').each ( index, item ) =>

			$item = $ item

			route = new Route
			
			route.id  = $item.attr 'id'
			route.url = $item.attr 'url'
			
			switch route.id

				when 'home'
					controller = require "controllers/home"

				when 'project'
					controller = require "controllers/project"
					
			route.controller = controller

			@routes.push route

		return @routes


	get_routes: ( index ) ->

		routes = []

		for route in @routes
			
			unless route.id is 'menu'
				
				routes.push route

		return routes


	get_route: ( index ) ->

		routes = do @get_routes

		return routes[index].url or '#'


	get_route_index: ( url ) ->

		routes = do @get_routes

		for route, index in routes
			
			if route.url.match url
				
				return index

				break

