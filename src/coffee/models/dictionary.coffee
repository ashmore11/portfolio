settings  = require 'app/settings'
happens   = require 'happens'
Loader    = require 'utils/loading/loader'

class Dictionary

	default_dictionary: 'dictionary.xml'

	constructor: ->

		happens @

		@$objs = {}

	
	load: ->

		loader = new Loader()

		loader.once 'loaded', ( manifest ) =>

			for asset in manifest
				
				@$objs[asset.id] = $ asset.data

			@emit 'loaded'

		loader.add 'dictionary.xml', "#{settings.base_path}/xml/dictionary.xml", 'xml'
		loader.add 'projects.xml',   "#{settings.base_path}/xml/projects.xml",   'xml'
		loader.add 'routes.xml',     "#{settings.base_path}/xml/routes.xml",     'xml'
		
		do loader.load
		

	get : ( id, dictionary = @default_dictionary ) =>
		
		node = @get_raw id, dictionary

		if node then return node.text() else return ''


	get_raw: ( id, dictionary = @default_dictionary ) ->

		if @$objs[ dictionary ].find( id ).length

			return @$objs[ dictionary ].find( id )

		return false


	get_dictionary: ( dictionary = 'dictionary.xml' ) ->
		
		return @$objs[ dictionary ]


module.exports = new Dictionary