settings   = require 'app/settings'
dictionary = require 'models/dictionary'

class Project

	id        : String
	assets    : Array
	copy      : Object

class Asset

	id   : String
	type : String
	src  : String

module.exports = new class Blogs

	projects: Array

	get: ->

		@projects = []

		xml = dictionary.get_dictionary 'projects.xml'

		xml.find('project').each ( i, project ) =>

			$project = $ project

			project_model = new Project
			
			project_model.index  = i
			project_model.id     = $project.attr('id')
			project_model.url    = $project.attr('url')
			project_model.video  = $project.find('video').attr('src')
			project_model.assets = []

			$project.find('asset').each ( i, asset ) =>

				$asset = $ asset

				asset_model = new Asset
				
				asset_model.id   = $asset.attr 'id'
				asset_model.type = $asset.attr 'type'
				asset_model.src  = $asset.attr 'src'

				project_model.assets.push asset_model

			project_model.copy =
				title       : $project.find('title').text()
				description : $project.find('description').text()
				link        : $project.find('external-link').text()
		
			@projects.push project_model

		return @projects

