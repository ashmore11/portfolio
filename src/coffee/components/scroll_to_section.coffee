win    = require 'utils/window'
utils  = require 'utils/utils'
device = require 'app/device'

module.exports = class Scroller

	scroll_target: 0
	is_tweening: false

	constructor: ( @el ) ->

		@footer = $ '#footer'

		@ui = 
			'section'  : '.section'
			'readmore' : '.read-more'

		@ui[ key ] = @el.find( el ) for key, el of @ui

		do @on_resize


	bind: ->

		win.on 'resize', @on_resize

		@ui.readmore.on 'click touchstart', @next_section

		$(document).on 'mousewheel DOMMouseScroll', @scroll_to_section


	unbind: ->

		win.off 'resize', @on_resize

		@ui.readmore.off 'click touchstart', @next_section

		$(document).off 'mousewheel DOMMouseScroll', @scroll_to_section


	next_section: ( event ) =>

		target = $ event.currentTarget

		@scroll_target = parseInt( target.parent().data 'section' ) + 1

		if @scroll_target is @ui.section.length
			if device.name is 'iPad'
				offset = @footer.offset().top - ( win.height - @footer.outerHeight() ) - 100
			else
				offset = @footer.offset().top - ( win.height - @footer.outerHeight() )
		else
			offset = $( @ui.section[ @scroll_target ] ).offset().top

		params =
			scrollTo: y: offset
			ease: Expo.easeInOut

		TweenLite.to window, 1, params

		@ui.readmore.find('.text').fadeOut()


	scroll_to_section: ( event ) =>

		event = event or window.event

		# Use default scroll for mobile layouts
		if win.width < 768 or device.name is 'iPad' then return

		do event.preventDefault

		if @is_tweening then return

		@is_tweening = true

		delta = event.originalEvent.detail < 0 or event.originalEvent.wheelDelta > 0 ? 1 : -1

		# Check scroll direction and update target
		if delta > 0
			--@scroll_target
		else
			++@scroll_target

		if @scroll_target is -1
			@scroll_target = 0

		if @scroll_target > @ui.section.length
			@scroll_target = @ui.section.length

		# Check if next target is the footer
		if @scroll_target is @ui.section.length
			offset = @footer.offset().top - ( win.height - @footer.outerHeight() )
		else
			offset = @el.find('.section[data-section="' + @scroll_target + '"]').offset().top

		params =
			scrollTo: y: offset
			ease    : Expo.easeInOut

		TweenLite.to window, 1, params

		utils.delay 1500, => @is_tweening = false


	on_resize: =>

		###
		Keep page at current section
		###
		unless win.width < 768 or device.name is 'iPad'
			
			if @scroll_target is @ui.section.length
				top = @footer.offset().top - ( win.height - @footer.outerHeight() )
			else
				top = $( @ui.section[ @scroll_target ] ).offset().top
			
			TweenLite.to window, 0, scrollTo: y: top