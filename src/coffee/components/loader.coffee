settings = require 'app/settings'
happens  = require 'happens'

class Loader

	duration: 0.6

	constructor: ->

		happens @

		@el      = $ '#loader'
		@spinner = @el.find '.percent'

	show: ->

		@emit 'show'

		$( '#loader .percent' ).html '0%'

		params =
			autoAlpha : 1
			onComplete: =>

				@emit 'half:shown'

				params =
					autoAlpha : 1
					onComplete: => @emit 'shown'

				TweenMax.to @spinner, @duration, params

		TweenMax.to @el, @duration, params

	hide: ->

		@emit 'hide'

		params =
			autoAlpha : 0
			delay     : 0.5
			onComplete: =>

				@emit 'half:hidden'

				params =
					autoAlpha : 0
					onComplete: => @emit 'hidden'

				TweenMax.to @el, @duration, params

		TweenMax.to @spinner, @duration, params


module.exports = new Loader