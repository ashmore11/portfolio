win        = require 'utils/window'
settings   = require 'app/settings'

module.exports = class VideoPlayer

	constructor: ( @el, @id ) ->

		@ui = 
			'play'       : '.play-button'
			'player'     : '#vid-player'

		@ui[ key ] = @el.find( el ) for key, el of @ui

		if settings.language is 'zh' then do @html_player else do @youtube_player


	html_player: ->

		@ui.play.on 'click', @play
		@ui.player.on 'click', @pause

		@player = videojs 'html-player'

		@player.on 'ended', @on_ended


	youtube_player: ->

		@ui.play.hide()

		if not YT?.Player.is_ready

			tag     = document.createElement 'script'
			tag.src = '//www.youtube.com/player_api'

			script_tag = document.getElementsByTagName('script')[0]
			script_tag.parentNode.insertBefore tag, script_tag

			window.onYouTubePlayerAPIReady = =>

				YT.Player.is_ready = true
				
				do @on_youtube_ready

		else

			do @on_youtube_ready


	on_youtube_ready: =>

		@player = new YT.Player 'vid-player',
			videoId: @id
			playerVars:
				showinfo: 0
				controls: 0
				rel: 0

		@player.addEventListener 'onStateChange', @state_changed


	play: =>

		if settings.language is 'zh'
			do @player?.play
		else
			do @player?.playVideo

		params = 
			autoAlpha: 0
			ease: Power4.easeOut

		TweenLite.to @ui.play,   0.75, params


	pause: =>

		if settings.language is 'zh'
			do @player?.pause
		else
			do @player?.pauseVideo

		params = 
			autoAlpha: 1
			ease: Power4.easeOut

		TweenLite.to @ui.play,   0.75, params


	get_state: ->

		@player?.getPlayerState()


	state_changed: ( event ) =>

		if event.data is 0 then do @on_ended

	
	on_ended: ->

		console.log 'VIDEO ENDED'