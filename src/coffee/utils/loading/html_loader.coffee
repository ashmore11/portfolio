happens	= require 'happens'

module.exports = class HTMLLoader

	constructor: -> happens @

	load: ( asset ) ->

		div = $ '<div>'

		div.load asset.src, =>
			asset.data = div
			@emit 'loaded', asset