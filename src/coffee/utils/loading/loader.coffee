happens	 	  = require 'happens'
DataLoader  = require 'utils/loading/data_loader'
ImageLoader = require 'utils/loading/image_loader'
HTMLLoader  = require 'utils/loading/html_loader'

###
Load files asynchronously
###
module.exports = class Loader

	constructor: ->

		happens @

		do @dispose

	add: (id, file, type, data) ->

		# c.log id, file, type

		obj =
			id   : id
			src  : file
			type : type
			data : data

		@manifest.push obj

	load: ->

		@count = 0
		@total = @manifest.length

		@date = new Date()

		for asset in @manifest

			switch asset.type

				when 'json', 'xml'
					l = new DataLoader
					l.once 'loaded', @success
					l.load asset

				when 'image'
					l = new ImageLoader
					l.once 'loaded', @success
					l.load asset

				when 'html'
					l = new HTMLLoader
					l.once 'loaded', @success
					l.load asset


	success: ( asset ) =>

		@count++

		percent = @count * 100 / @manifest.length

		@emit 'loading', percent

		if @count >= @total

			# c.debug 'util::loader --> Loaded assets in', (new Date() - @date) / 1000

			@emit 'loaded', @manifest

	error: ( error ) =>

		c.log 'error', error


	get_asset: ( id ) ->
		result = false
		for asset in @manifest
			if asset.id.match id
				result = asset

		return result

	dispose: ->

		@manifest = []
