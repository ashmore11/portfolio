happens	= require 'happens'

module.exports = class Request

	constructor: -> happens @

	get: ( params ) ->

		c.log params

		$.ajax
			type: 'GET'
			url: params.url
			data: params.data
			success: ( result ) => 

				@emit 'done', JSON.parse result
			
			error: ( error ) =>

				@emit 'error', error

	post: ( params ) ->

		c.log params

		$.ajax
			type: 'POST'
			url: params.url
			data: params.data
			processData: off
			success: ( result ) => 

				@emit 'done', result
			
			error: ( error ) =>

				@emit 'error', error