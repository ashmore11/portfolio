happens = require 'happens'

class Window

	window       : $ window
	body         : $ 'body'
	width        : 0
	height       : 0
	scroll_timer : null
	scroll_top   : 0

	# Responsive booleans
	desktop 	 : off
	tablet  	 : off
	mobile  	 : off

	constructor: ->

		happens @

		@window.on 'resize', @resize
		@window.on 'scroll', @scroll
		@window.on 'keydown', @keydown

		do @resize

	resize: =>

		@width  = @window.width()
		@height = @window.height()

		# Set responsive state flags
		@mobile  = off
		@tablet  = off
		@desktop = off

		if @width <= 767
			@mobile = on
		else if @width <= 1279
			@tablet = on
		else
			@desktop = on

		@emit 'resize'

	scroll: ( event ) =>

		clearTimeout @scroll_timer


		unless @body.hasClass 'scrolling'
			@body.addClass 'scrolling'

		@scroll_timer = setTimeout( => 

			@body.removeClass 'scrolling'
					
		, 50 )

		scroll_top = @window.scrollTop()

		if scroll_top > @scroll_top
			direction = 'down'
		else
			direction = 'up'

		@scroll_top = scroll_top

		@emit 'scroll', scroll_top, direction

		do event.preventDefault


	keydown: ( event ) =>

		# if event.keyCode is 83

		key = event.keyCode

		switch event.keyCode

			when 38
				key = 'up'

			when 40
				key = 'down'

			when 37
				key = 'left'

			when 39
				key = 'right'

		@emit "keydown", key

module.exports = new Window