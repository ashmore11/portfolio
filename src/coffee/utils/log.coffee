log = {}

log.enable = false

log.clear = () ->

	if console? and console.clear?
		console.clear()

log.log = (args...) ->

	if @enable
		if console? and console.log? and console.log.apply?
			console.log args...
		else
			console.log args

log.debug = (args...) ->

	if @enable
		if console? and console.debug? and console.debug.apply?
			console.debug args...
		else
			console.log args

log.info = (args...) ->

	if @enable
		if console? and console.info? and console.info.apply?
			console.info args...
		else
			console.log args

log.warn = (args...) ->

	if @enable
		if console? and console.warn? and console.warn.apply?
			console.warn args...
		else
			console.log args

log.error = (args...) ->

	if @enable
		if console? and console.error? and console.error.apply?
			console.error args...
		else
			console.log args

log.group = (args...) ->

	if @enable
		if console? and console.group? and console.group.apply?
			console.group args...
		else
			console.group args


log.groupEnd = (args...) ->

	if @enable
		if console? and console.groupEnd? and console.groupEnd.apply?
			console.groupEnd args...
		else
			console.groupEnd args

log.table = (args...) ->

	if @enable
		if console? and console.table? and console.table.apply?
			console.table args...
		else
			log.log args

module.exports = log