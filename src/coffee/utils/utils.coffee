module.exports =

	lerp: ( min, max, ratio ) ->
		min + ((max - min) * ratio)

	delay: ( delay, func ) ->
		setTimeout  func, delay

	open_window: ( url, w, h ) =>

		left = ( screen.availWidth  - w ) >> 1
		top  = ( screen.availHeight - h ) >> 1

		window.open url, '', "top=#{top},left=#{left},width=#{w},height=#{h},location=no,menubar=no"

		return off

	###
	https://gist.github.com/svlasov-gists/2383751
	###
	merge: (target, source) ->
		
		# Merges two (or more) objects,
		# giving the last one precedence 
		
		target = {}  if typeof target isnt "object"
		
		for property of source
			if source.hasOwnProperty(property)
				sourceProperty = source[property]
				if typeof sourceProperty is "object"
					target[property] = @merge(target[property], sourceProperty)
					continue
				target[property] = sourceProperty
		a = 2
		l = arguments.length

		while a < l
			merge target, arguments[a]
			a++
		
		target

Number::map = (in_min, in_max, out_min, out_max, val) ->
	(this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min