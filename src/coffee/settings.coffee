module.exports = new class

	constructor: ->

		@base_path  = env.BASE_PATH
		@language   = env.LANGUAGE
		@debug 	    = env.DEBUG
		@form_url   = env.FORM_POST_URL
		@transforms = Modernizr.csstransforms
		@mobile     = 768