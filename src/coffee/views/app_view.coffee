happens  = require 'happens'
settings = require 'app/settings'
loader   = require 'components/loader'

module.exports = class AppView

	constructor: ( @el, @data = null ) ->

		happens @

		@logo = $('#header').find '.logo img'

		do @animate_logo


	bind: ->

		$('body').find( 'a[href^="/"]' ).on 'click touchstart', @on_click


	unbind: ->

		$('body').find( 'a[href^="/"]' ).off 'click touchstart', @on_click


	animate_logo: ->

		loader.on 'show', =>

			@logo.addClass 'loading'

			loader.on 'half:hidden', =>

				@logo.one 'animationiteration webkitAnimationIteration', =>

					@logo.removeClass 'loading'


	on_click:	( event ) =>

		do event.preventDefault

		$('#header').find('li a').removeClass 'current'

		el = $ event.currentTarget

		el.addClass 'current'

		router = require 'controllers/router'
		router.go el.attr 'href'

	
	destroy: ->

		do @unbind

		do @el.remove

		@emit 'destroyed'