win      = require 'utils/window'
loader   = require 'components/loader'
settings = require 'app/settings'

AppView = require 'views/app_view'

module.exports = class Header extends AppView

	constructor: ( @el ) ->

		url    = window.location.pathname.replace settings.base_path, ''
		pieces = url.split('/')
		id     = pieces[ pieces.length - 1 ]

		if id is 'about' then $('#header').find('li.about a').addClass 'current'
		if id is ''      then $('#header').find('li.work a').addClass 'current'

		loader.on 'hidden', => 
			$('#loader .spinner').hide()
			TweenLite.to @el.find('.header'), 1, autoAlpha: 1

		do @bind
		do @on_resize

	
	bind: ->

		super()

		win.on 'resize', @on_resize


	unbind: ->

		super()

		win.off 'resize', @on_resize


	on_resize: =>

		#
