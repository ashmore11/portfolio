win     = require 'utils/window'
utils   = require 'utils/utils'
AppView = require 'views/app_view'

module.exports = class Home extends AppView

	start: ( @el ) ->

		@ui =
			'project'      : 'li.project'
			'project_link' : 'li.project a'
		
		@ui[ key ] = @el.find( el ) for key, el of @ui

		do @on_resize
		do @bind


	bind: ->

		super()

		win.on 'resize', @on_resize

		@ui.project_link.on 'click', @on_project_clicked


	unbind: ->

		super()

		win.off 'resize', @on_resize

		@ui.project_link.off 'click', @on_project_clicked


	on_project_clicked: ( event ) =>

		do event.preventDefault

		target = $ event.currentTarget

		el = @ui.project_link.parents('li')

		for i in [ 0..@ui.project.length ]

			params =
				opacity   : 0
				delay     : 0.1 * i
				ease      : Cubic.easeOut
				onComplete: ->

			TweenLite.to $( el[ i ] ), 1, params


	on_resize: =>

		@el.find('li').height win.height / 2

