win     = require 'utils/window'
utils   = require 'utils/utils'
Masonry = require 'masonry-layout'
AppView = require 'views/app_view'

module.exports = class Project extends AppView

	start: ( @el ) ->

		@ui =
			'images' : '#images'
			'image'  : '.image'
		
		@ui[ key ] = @el.find( el ) for key, el of @ui

		do @create_masonry_grid
		do @on_resize
		do @bind


	bind: ->

		super()

		win.on 'resize', @on_resize


	unbind: ->

		super()

		win.off 'resize', @on_resize


	create_masonry_grid: ->

		@masonry = new Masonry '#images',
			itemSelector       : '.image'
			columnWidth        : 0
			gutterWidth        : 20
			transitionDuration : 0

		@ui.image.each ( index, item ) =>

			if $( item ).position().left is 0

				$(item).css paddingRight: 10

			else

				$(item).css paddingLeft: 10


	on_resize: =>

		@masonry.columnWidth = ( @ui.images.width() / 2 )