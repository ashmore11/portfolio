win     = require 'utils/window'
AppView = require 'views/app_view'

module.exports = class Footer extends AppView

	constructor: ( @el ) ->

		do @bind
		do @on_resize


	bind: ->

		super()

		win.on 'resize', @on_resize


	unbind: ->

		super()

		win.off 'resize', @on_resize


	on_resize: =>

		#
