shjs = require 'shelljs/global'

path = '../public/images/register/'

stories  = off
masthead = on
products = off

images = [
	'story-4.png'
]

images = [
	'masthead.jpg'
]

images = [
	'product-1.jpg'
	'product-2.jpg'
	'product-3.jpg'
	'product-4.jpg'
	'product-5.jpg'
	'product-6.jpg'
	'product-7.jpg'
	'product-8.jpg'
	'product-9.jpg'
]

cmds = []

for image, i in images

	src = path + image

	# split = '.jpg'
	split = '.png'

	if stories

		# desktop
		dest = src.split(split)[0] + '-desktop.jpg'
		cmds.push "convert -resize 540x400 #{src} #{dest}"

		dest = src.split(split)[0] + '-desktop@2x.jpg'
		cmds.push "convert -resize 1080x800 #{src} #{dest}"

		# tablet
		dest = src.split(split)[0] + '-tablet.jpg'
		cmds.push "convert -resize 384x284 #{src} #{dest}"

		dest = src.split(split)[0] + '-tablet@2x.jpg'
		cmds.push "convert -resize 768x568 #{src} #{dest}"

		# mobile
		dest = src.split(split)[0] + '-mobile.jpg'
		cmds.push "convert -resize 320x237 #{src} #{dest}"

		dest = src.split(split)[0] + '-mobile@2x.jpg'
		cmds.push "convert -resize 640x474 #{src} #{dest}"

	# cmds.push "rm #{src}"

	# masthead

	if masthead

		# desktop
		dest = src.split(split)[0] + '-desktop.jpg'
		cmds.push "convert -resize 1440x400 #{src} #{dest}"

		dest = src.split(split)[0] + '-desktop@2x.jpg'
		cmds.push "convert -resize 2880x800 #{src} #{dest}"

		# tablet
		dest = src.split(split)[0] + '-tablet.jpg'
		cmds.push "convert -resize 1024x284 #{src} #{dest}"

		dest = src.split(split)[0] + '-tablet@2x.jpg'
		cmds.push "convert -resize 2048x568 #{src} #{dest}"

		# mobile
		dest = src.split(split)[0] + '-mobile.jpg'
		cmds.push "convert -resize 320x237 #{src} #{dest}"

		dest = src.split(split)[0] + '-mobile@2x.jpg'
		cmds.push "convert -resize 640x474 #{src} #{dest}"

	if products

		# desktop
		dest = src.split(split)[0] + '-desktop.jpg'
		cmds.push "convert -resize 360x300 #{src} #{dest}"

		dest = src.split(split)[0] + '-desktop@2x.jpg'
		cmds.push "convert -resize 720x600 #{src} #{dest}"

		# tablet
		dest = src.split(split)[0] + '-tablet.jpg'
		cmds.push "convert -resize 256x213 #{src} #{dest}"

		dest = src.split(split)[0] + '-tablet@2x.jpg'
		cmds.push "convert -resize 512x426 #{src} #{dest}"

		# mobile
		dest = src.split(split)[0] + '-mobile.jpg'
		cmds.push "convert -resize 320x237 #{src} #{dest}"

		dest = src.split(split)[0] + '-mobile@2x.jpg'
		cmds.push "convert -resize 640x474 #{src} #{dest}"

# echo cmds
# return

for cmd in cmds

	exec cmd, ( code, output ) ->

		echo 'done'