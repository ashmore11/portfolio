shjs = require 'shelljs/global'

path = '../public/images/history/2014/'


cmds = []

for i in [0...10]

	src  = path + 'desktop.jpg'
	dest = path + "story-#{i}-desktop.jpg"
	cmds.push "cp #{src} #{dest}"

	src  = path + 'desktop@2x.jpg'
	dest = path + "story-#{i}-desktop@2x.jpg"
	cmds.push "cp #{src} #{dest}"

	src  = path + 'tablet.jpg'
	dest = path + "story-#{i}-tablet.jpg"
	cmds.push "cp #{src} #{dest}"

	src  = path + 'tablet@2x.jpg'
	dest = path + "story-#{i}-tablet@2x.jpg"
	cmds.push "cp #{src} #{dest}"

	src  = path + 'mobile.jpg'
	dest = path + "story-#{i}-mobile.jpg"
	cmds.push "cp #{src} #{dest}"

	src  = path + 'mobile@2x.jpg'
	dest = path + "story-#{i}-mobile@2x.jpg"
	cmds.push "cp #{src} #{dest}"

echo cmds
# return

for cmd in cmds

	exec cmd, ( code, output ) ->

		echo 'done'