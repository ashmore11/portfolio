require 'shelljs/global'

# Zip the public folder up

date = new Date()

second  = date.getSeconds()
hour    = date.getHours()
minute  = date.getMinutes()
day     = date.getDay()
month   = date.getMonth()
year    = date.getFullYear()

archive_name = 'hogan_com_' + hour + '-' + minute + '-' + second + '-' + day + '_' + month + '_' + year

path = './builds'

cmd = "zip -r #{path}/#{archive_name}.zip public/*"

echo 'release id: ' + archive_name

exec cmd, (code, output) ->

	echo 'code', code
	echo 'output', output