setup:
	npm install

watch:
	# open http://localhost:9001
	NODE_ENV=development gulp

spritesheet:
	cd ./gulp/assets && NODE_ENV=production gulp

release:
	NODE_ENV=production gulp build

zip:
	# make release
	shjs scripts/release

staging:
	make release
	cd public && rsync -avz -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress . root@devil.hi-res.net:/var/www/hogan/busybeautiful