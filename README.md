# Hogan.com

## Setup

```
make setup
```

## Development

```
make watch
```

## Release

```
make release
```
