path 		    = require 'path'
gulp 		    = require 'gulp'
livereload  = require 'gulp-livereload'
jade        = require 'gulp-jade'
handleError = require '../util/handle_error'

production = process.env.NODE_ENV is 'production'

exports.paths =
	source      : './src/jade/*.jade'
	watch       : './src/jade/**/*.jade'
	destination : './public'

gulp.task 'templates', ->
	pipeline = gulp
		.src exports.paths.source
		.pipe jade pretty: not production
		.on 'error', handleError
		.pipe gulp.dest exports.paths.destination

	pipeline = pipeline.pipe livereload() unless production

	pipeline