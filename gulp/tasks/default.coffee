gulp = require 'gulp'

gulp.task "build", [
	'scripts', 
	'vendor_js',
	'styles',
	'templates'
]

gulp.task "default", [
	'build',
	'watch',
	'server'
]