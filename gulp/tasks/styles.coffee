config      = require '../../package.json'
gulp 	    = require 'gulp'
livereload  = require 'gulp-livereload'
stylus      = require 'gulp-stylus'
nib 	    = require 'nib'
CSSmin      = require 'gulp-minify-css'
source      = require 'vinyl-source-stream'
prefix      = require 'gulp-autoprefixer'
handleError = require '../util/handle_error'
rename 		= require "gulp-rename"

development = process.env.NODE_ENV is 'development'
production  = process.env.NODE_ENV is 'production'

exports.paths =
	source: './src/stylus/app.styl'
	watch: './src/stylus/**/*.styl'
	destination: './public/css/'
	release: "app.min.#{config.version}.css"

gulp.task 'styles', ->

	styles = gulp
		.src exports.paths.source
		.pipe(stylus({
			set: ['include css']
			use: nib()
			linenos: development
		}))
		.on 'error', handleError

	styles = styles.pipe(CSSmin()) if production
	styles = styles.pipe gulp.dest exports.paths.destination
	styles = styles.pipe livereload() unless production

	if production
		gulp.src(exports.paths.destination + 'app.css')
			.pipe(rename(exports.paths.release))
			.pipe(gulp.dest(exports.paths.destination))