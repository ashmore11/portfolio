config      = require '../../package.json'
gulp        = require 'gulp'
streamify   = require 'gulp-streamify'
uglify      = require 'gulp-uglify'
concat      = require 'gulp-concat'
nib 	      = require 'nib'
CSSmin      = require 'gulp-minify-css'
stylus      = require 'gulp-stylus'
handleError = require '../util/handle_error'
prefix      = require 'gulp-autoprefixer'
livereload  = require 'gulp-livereload'

development = process.env.NODE_ENV is 'development'
production  = process.env.NODE_ENV is 'production'

exports.paths_js =
	source      : './src/vendor/**/*.js'
	destination : './public/js/'
	filename    : 'vendor.js'
	release     : "vendor.min.#{config.version}.js"

gulp.task 'vendor_js', ->

	if production
		filename = exports.paths_js.release
	else
		filename = exports.paths_js.filename

	vendor = gulp
				.src exports.paths_js.source
				.pipe concat filename
	
	vendor.pipe(uglify()) if production
	vendor.pipe gulp.dest exports.paths_js.destination

	vendor