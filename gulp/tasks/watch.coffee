gulp 	    = require 'gulp'
livereload  = require 'gulp-livereload'
watchify    = require 'watchify'
source      = require 'vinyl-source-stream'
handleError = require '../util/handle_error'

production = process.env.NODE_ENV is 'production'

# Files to watch
paths = 
	templates  : require('./templates').paths
	styles     : require('./styles').paths
	vendor_js  : require('./vendor').paths_js
	scripts    : require('./scripts').paths

gulp.task "watch", ->
	livereload.listen()

	gulp.watch paths.templates.watch,  ['templates']
	gulp.watch paths.styles.watch,     ['styles']
	gulp.watch paths.vendor_js.watch,  ['vendor_js']

	bundle = watchify
		entries: [paths.scripts.source]
		extensions: ['.coffee']

	bundle.on 'update', ->
		build = bundle.bundle(debug: not production)
			.on 'error', handleError

			.pipe source paths.scripts.filename

		build
			.pipe gulp.dest paths.scripts.destination
			.pipe(livereload())

	.emit 'update'