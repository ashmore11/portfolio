config 		  = require '../../package.json'
path 		    = require 'path'
gulp 		    = require 'gulp'
streamify   = require 'gulp-streamify'
uglify      = require 'gulp-uglify'
browserify  = require 'browserify'
source      = require 'vinyl-source-stream'
handleError = require '../util/handle_error'

production = process.env.NODE_ENV is 'production'

exports.paths =
	source      : './src/coffee/app.coffee'
	destination : './public/js/'
	filename    : 'app.js'
	release     : "app.min.#{config.version}.js"

gulp.task 'scripts', ->

	if production
		filename = exports.paths.release
	else
		filename = exports.paths.filename

	bundle = browserify
		entries: [exports.paths.source]
		extensions: ['.coffee']

	build = bundle.bundle(debug: not production)
		.on 'error', handleError
		.pipe source filename

	build.pipe(streamify(uglify())) if production

	build
		.pipe gulp.dest exports.paths.destination